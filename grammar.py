# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.


class GrammarInstance:
    def __init__(self, rule_filename):
        """
        Initialises an instance ruled by the given grammar.
        :param rule_filename: The file containing the grammar. Each line should
               be written as [element] -> [production].
        """
        self.rules = {}
        with open(rule_filename) as rule_file:
            for row in rule_file:
                elements = [e.strip() for e in row.split("->")]
                if len(elements) != 2 or any([len(e) <= 0 for e in elements]):
                    raise AttributeError("Invalid rule: %s" % row)
                if elements[0] in self.rules:
                    raise AttributeError("Ambiguous rule: %s" % row)
                self.rules[elements[0]] = elements[1]

        if len(self.rules) == 0:
            raise AttributeError("Empty grammar.")

        self.state = str()
        self.reset()

    def step(self):
        """
        Develops the current state according to the grammar.
        :return: None
        """
        future = str()
        for i in range(len(self.state)):
            if self.state[i] in self.rules.keys():
                rule = self.rules[self.state[i]]
                future += rule
            else:
                future += self.state[i]
        self.state = future

    def reset(self):
        """
        Resets the instance's state to the grammar's axiom.
        :return: None
        """
        self.state = next(iter(self.rules.keys()))

    def __str__(self):
        """
        Returns the current state of the instance.
        :return: str The state as a string.
        """
        return self.state
