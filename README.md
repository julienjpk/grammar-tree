# Grammar-inferred tree #

Simple GUI displaying a tree the structure of which is based on the following 
grammar:

    A -> C[B]D
    B -> A
    C -> C
    D -> C(E)A
    E -> D
    
If I remember correctly:

- `[` means *branch right*.
- `(` means *branch left*.
- `]` and `)` cancel the last branching, reverting to the previous angle.
- Other characters just make the current branch grow a bit.
    
This is pretty much useless but I think it looks kinda neat so... Here it is. 
You can run it as:
 
    ./grammar_tree.py grammar.dat

- Press `space` to bring the sentence to its next step.
- Press `p` to print the current sentence to standard output.

You can also play a bit with the parameters to `TreeEvolution()`.
