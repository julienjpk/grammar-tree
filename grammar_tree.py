#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

from grammar import *

from tkinter import *
import sys
import math as m
import random as r


class TreeEvolution(Frame):
    def __init__(self, master: Tk, grammar, width=300, height=300,
                 start_at=(0.5, 1), initial_direction=m.pi/2, angle_step=m.pi/6,
                 growth=40, thickness=10, g_keep=0.8, t_keep=0.5, wind=(0, 0)):
        """
        Prepares the tree view.
        :param master: The Tk master.
        :param grammar: The GrammarInstance object.
        :param width: The canvas' width.
        :param height: The canvas' height.
        :param start_at: The relative coordinates for the starting point.
        :param initial_direction: The initial direction (angle).
        :param angle_step: The angle variation at each deviation.
        :param growth: The line length (branches).
        :param thickness: The line thickness (branches).
        :param g_keep: How much growth should be kept as the stack grows.
        :param t_keep: How much thickness should be kept as the stack grows.
        :param wind: The effect of the wind (angle_variation, probability).
        """
        Frame.__init__(self, master)

        # Checking the tuples.
        if not isinstance(start_at, tuple) or len(start_at) != 2:
            raise ValueError("Invalid starting point: %r" % start_at)
        if not isinstance(wind, tuple) or len(wind) != 2:
            raise ValueError("Invalid wind effect: %r" % wind)

        # Setting the parameters for paint_canvas.
        self.grammar = grammar
        self.start_at = start_at
        self.direction = initial_direction
        self.angle_step = angle_step
        self.growth = growth
        self.thickness = thickness
        self.g_keep = g_keep
        self.t_keep = t_keep
        self.wind = wind

        # Creating the canvas and binding events.
        self.canvas = Canvas(master, width=width, height=height)
        self.canvas.pack(fill=BOTH, expand=YES)
        self.canvas.bind("<Configure>", self.paint_canvas)
        self.master.bind("<space>", self.on_spacebar)
        self.master.bind("<p>", self.print_grammar)
        self.paint_canvas()

    def paint_canvas(self, e=None):
        """
        Paints the canvas based on the grammar's current state.
        :param e: The Tk event (unused).
        :return: None
        """
        nodes = []
        direction = self.direction

        # Determine the starting point.
        width = self.canvas.winfo_width()
        height = self.canvas.winfo_height()
        current_at = (width * self.start_at[0], height * self.start_at[1])

        # Clear everything.
        self.canvas.delete("all")
        self.canvas.create_rectangle(0, 0, width, height, fill="#666")

        for character in self.grammar.__str__():
            # Case 1: the character creates a deviation.
            if character in ('[', '('):
                nodes.append((current_at + (), direction))

                wind_effect = self.wind[0] if r.random() < self.wind[1] else 0
                wind_effect *= -1 if r.random() < 0.5 else 1
                actual_angle = self.angle_step + wind_effect

                direction += (-1 if character == '[' else 1) * actual_angle

            # Case 2: the character cancels the latest deviation.
            elif character in (']', ')'):
                current_at, direction = nodes.pop()

            # Case 3: the character extends the tree.
            else:
                line_length = max(1, self.growth * pow(self.g_keep, len(nodes)))
                line_width = max(1, self.thickness * pow(self.t_keep,
                                                         len(nodes)))
                colour = ("%02x" % (175 * pow(0.9, len(nodes)))) * 3

                x = int(round(current_at[0] + line_length * m.cos(direction)))
                y = int(round(current_at[1] - line_length * m.sin(direction)))
                self.canvas.create_line(current_at[0], current_at[1], x, y,
                                        width=line_width, fill="#" + colour)
                current_at = (x, y)

    def on_spacebar(self, e):
        """
        Applies the grammar and draws the canvas again (press the space bar).
        :param e: The Tk event (unused).
        :return: None
        """
        self.grammar.step()
        self.paint_canvas()

    def print_grammar(self, e):
        """
        Dumps the current grammar to standard input.
        :param e: The Tk event (unused).
        :return: None
        """
        print("%s" % self.grammar)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: %s [grammar]." % sys.argv[0])
        sys.exit(1)

    try:
        grammar = GrammarInstance(sys.argv[1])
    except Exception as e:
        print("Error while parsing the grammar file.\n%s" % e)
        sys.exit(1)

    master = Tk()
    tree = TreeEvolution(master, grammar,
                         start_at=(0.5, 0.95),
                         wind=(1.0/6, 0.8))
    tree.mainloop()
